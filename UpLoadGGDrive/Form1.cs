﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System.IO;
using System.Threading;


namespace UploadGGDrive
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
            Control.CheckForIllegalCrossThreadCalls = false;
        }
        static string[] Scopes = { DriveService.Scope.Drive };
        static string ApplicationName = "UpLoadGGDrive";
        private string checkName = null;
        private string savedId = null;

        private void ListFiles(DriveService service, ref string pageToken)
        {
            // Define parameters of request.
            FilesResource.ListRequest listRequest = service.Files.List();
            listRequest.PageSize = 1;
            listRequest.Fields = "nextPageToken, files(name)";
            listRequest.Q = "mimeType=''";

            // List files.
            var request = listRequest.Execute();


            if (request.Files != null && request.Files.Count > 0)
            {


                foreach (var file in request.Files)
                {
                    textBox1.Text += string.Format("{0}", file.Name);
                }

                pageToken = request.NextPageToken;

                if (request.NextPageToken != null)
                {

                    Console.ReadLine();

                }

            }
            else
            {
                textBox1.Text += ("\r\nEverything good.");
            }
        }
        private void UploadGG(string path, DriveService service, string folderUpload)
        {
            var fileMetadata = new Google.Apis.Drive.v3.Data.File();
            fileMetadata.Name = Path.GetFileName(path);
            //fileMetadata.MimeType = "image/*";
            fileMetadata.MimeType = "";

            fileMetadata.Parents = new List<string>
            {
                folderUpload
            };


            FilesResource.CreateMediaUpload request;
            using (var stream = new System.IO.FileStream(path, System.IO.FileMode.Open))
            {

                //request = service.Files.Create(fileMetadata, stream, "image/*");
                request = service.Files.Create(fileMetadata, stream, "");
                request.Fields = "id";
                request.Upload();
            }
            //txtFileSelected.Text += "test...";

            var file = request.ResponseBody;
            //textBox1.Text += ("File ID: " + file.Id);

        }
        private UserCredential GetCredentials()
        {
            UserCredential credential;

            using (var stream = new FileStream("client_secret.json", FileMode.Open, FileAccess.Read))
            {
                string credPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

                credPath = Path.Combine(credPath, "client_secreta.json");

                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.FromStream(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                textBox1.Text = string.Format("Credential file saved to: " + credPath);
            }

            return credential;
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void UploadToGG(DriveService service, string folderId)
        {
            DialogResult = MessageBox.Show("Are you sure to upload?", "Confirm", MessageBoxButtons.YesNo);
            if (DialogResult == DialogResult.Yes)
            {
                progressBar1.Minimum = 0;
                progressBar1.Maximum = openFileDialog1.FileNames.Length * 10;
                progressBar1.Value = 5;
                progressBar1.Step = 10;

                txtFileSelected.Text = "Uploading files to folder: => "
                    + txtFolderNameUpload.Text + "\r\n";
                txtFileSelected.Text += "Application is running in the background, please wait..." + "\r\n\r\n";

                foreach (string filename in openFileDialog1.FileNames)
                {

                    //txtFileSelected.Text += "Test...";
                    Thread thread = new Thread(() =>
                    {
                        //txtFileSelected.Text += "Loading file " +filename + "...";
                        UploadGG(filename, service, folderId);
                        txtFileSelected.Text += filename + " => Upload successful ..." + "\r\n";
                        //progressBar1.PerformStep();
                    });
                    thread.IsBackground = true;
                    thread.Start();
                    thread.Join();
                    progressBar1.PerformStep();
                    if (progressBar1.Value == openFileDialog1.FileNames.Length * 10)
                    {
                        MessageBox.Show("Upload completed!");
                    }
                }

            }
            else
            {
                txtFileSelected.Text += "\n********************\r\nCancel upload!\r\n********************\r\n";
            }
        }

        private void btnBrowse_Click_1(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtFileSelected.Text = "\n********************\r\nFiles selected\r\n********************\r\n";
                foreach (string filename in openFileDialog1.FileNames)
                {
                    txtFileSelected.Text += filename + "\r\n";
                }
                button1.Enabled = true;
            }
            else
            {
                txtFileSelected.Text = "\n********************\r\nNo files selected!\r\n********************\r\n";
            }

        }
        private void txtFolderNameUpload_TextChanged(object sender, EventArgs e)
        {
            btnBrowse.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            UserCredential credential;
            credential = GetCredentials();

            // Create Drive API service.
            var service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });
            string folderid;

            if (checkName != txtFolderNameUpload.Text)
            {
                checkName = txtFolderNameUpload.Text;
                //get folder id by name
                var fileMetadatas = new Google.Apis.Drive.v3.Data.File()
                {
                    Name = txtFolderNameUpload.Text,
                    MimeType = "application/vnd.google-apps.folder"
                };
                var requests = service.Files.Create(fileMetadatas);
                requests.Fields = "id";
                var files = requests.Execute();
                folderid = files.Id;
                savedId = files.Id;
                UploadToGG(service, folderid);
            }
            else
            {
                UploadToGG(service, savedId);
            }


            string pageToken = null;

            do
            {
                ListFiles(service, ref pageToken);

            } while (pageToken != null);

            textBox1.Text += " Connected to Google Drive . . .";
        }

    }
}